# publish-drive-java

Application that uses google SDK to get a file from my Google Drive and publish it on a public GCS bucket.
As java is not supported in Functions. We are using the beta service Google Run which allows us to run a container in a serverless way.

#### env
1. Create a service account and get credentials file.
2. Set credentials env var
    
    GOOGLE_APPLICATION_CREDENTIALS=/Users/holivera/Documents/publish-drive-app/creds.json
    
3. Get project ID:
    
    gcloud config get-value project        

#### Build 

* Create a Dockerfile. This custom file uses a temporal container to build the artifacts.

* Create image
    
```sh 
gcloud builds submit --tag gcr.io/PROJECT-ID/publish-drive
```

* Config

    - Get the drive folder ID. e.g. abctWyGf9poH123
    - Get the name of the bucket. e.g. my-bucket-name

* Deploy

```sh 
gcloud run deploy --image  gcr.io/PROJECT-ID/publish-drive --platform managed --update-env-vars APP_DRIVE_PARENT_FOLDER=my-drive-folder, APP_GCS_BUCKET=my-bucket-name
```

* Validate

    In order to validate the deployed app, obtain an authorization bearer token.
    
```sh 
gcloud auth print-identity-token
```


     
