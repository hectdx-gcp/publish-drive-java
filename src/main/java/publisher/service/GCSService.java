package publisher.service;

import com.google.cloud.storage.Acl;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;


public class GCSService {

    private static final Logger logger = LoggerFactory.getLogger(GCSService.class);

    public static void uploadFile(final String bucketName, final String finalName, ByteArrayOutputStream outputStream) throws IOException {

        if (outputStream == null) {
            logger.info("File stream not found!");
            return;
        }

        logger.info("Uploading file...");

        Storage storage = StorageOptions.getDefaultInstance().getService();

        try (InputStream is = new ByteArrayInputStream(outputStream.toByteArray())) {

            BlobInfo blobInfo =
                    storage.create(
                            BlobInfo
                                    .newBuilder(bucketName, finalName)
                                    .setAcl(Arrays.asList(Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER)))
                                    .build(),
                            is);

            logger.info("link: {}", blobInfo.getSelfLink());
            logger.info("done!");
        }
    }
}
