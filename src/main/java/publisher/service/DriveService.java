package publisher.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;


public class DriveService {

    private static final Logger logger = LoggerFactory.getLogger(DriveService.class);

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final List<String> SCOPES = Arrays.asList(DriveScopes.DRIVE);


    public static ByteArrayOutputStream downloadFile(String appName, String parentFolder) throws IOException, GeneralSecurityException {

        logger.info("Downloading ...");
        GoogleCredential cr = GoogleCredential.getApplicationDefault().createScoped(SCOPES);

        logger.info("Creds from: {} - {} ", cr.getServiceAccountId(), cr.getServiceAccountUser());

        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, cr)
                .setApplicationName(appName)
                .build();


        String filterDate = LocalDate.now().minusMonths(1).toString();
        String query = String.format("'%s' in parents and createdTime > '%sT00:00:00' ", parentFolder, filterDate);

        logger.info("Using query: {}", query);

        FileList result = service.files().list()
                .setQ(query)
                .setPageSize(3)
                .setFields("nextPageToken, files(id, name, createdTime)")
                .execute();

        List<File> files = result.getFiles();
        if (files == null || files.isEmpty()) {
            logger.info("No files found.");
        } else {
            logger.info("Files:");
            for (File file : files) {
                logger.info("{} ({}) {}", file.getName(), file.getId(), file.getCreatedTime());
            }


            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {

                logger.info("Getting first file: {}", files.get(0).getName());

                service.files().get(files.get(0).getId())
                        .executeMediaAndDownloadTo(outputStream);


                return outputStream;

            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }

        }


        return null;
    }


}
