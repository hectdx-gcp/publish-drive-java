package publisher.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import publisher.CustomProperties;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;

@Component
public class PublishHelper {

    @Autowired
    private CustomProperties properties;

    public void publish() throws IOException, GeneralSecurityException {
        //download from drive
        ByteArrayOutputStream outputStream = DriveService.downloadFile(properties.getAppName(), properties.getDrive().getParentFolder());

        //upload to storage
        GCSService.uploadFile(properties.getGcs().getBucket(), properties.getGcs().getFinalName(), outputStream);

    }
}
