package publisher;

import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "app")
public class CustomProperties {

    private String appName;
    private Drive drive;
    private GCS gcs;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Drive getDrive() {
        return drive;
    }

    public void setDrive(Drive drive) {
        this.drive = drive;
    }

    public GCS getGcs() {
        return gcs;
    }

    public void setGcs(GCS gcs) {
        this.gcs = gcs;
    }

    public static class Drive {
        private String parentFolder;

        public String getParentFolder() {
            return parentFolder;
        }

        public void setParentFolder(String parentFolder) {
            this.parentFolder = parentFolder;
        }
    }

    public static class GCS {
        private String bucket;
        private String finalName;

        public String getBucket() {
            return bucket;
        }

        public void setBucket(String bucket) {
            this.bucket = bucket;
        }

        public String getFinalName() {
            return finalName;
        }

        public void setFinalName(String finalName) {
            this.finalName = finalName;
        }
    }
}
