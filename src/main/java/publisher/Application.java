package publisher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@SpringBootApplication
@EnableConfigurationProperties(CustomProperties.class)
public class Application {


    /**
     * Credentials file path needs to be specified with GOOGLE_APPLICATION_CREDENTIALS env var
     */

    public static void main(String... args) {


        SpringApplication.run(Application.class, args);


    }


}
