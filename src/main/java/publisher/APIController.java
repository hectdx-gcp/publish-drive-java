package publisher;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import publisher.service.PublishHelper;

import java.io.IOException;
import java.security.GeneralSecurityException;

@RestController
public class APIController {

    private static final Logger logger = LoggerFactory.getLogger(APIController.class);

    @Autowired
    private PublishHelper publishHelper;


    @GetMapping
    ResponseEntity<String> publish() throws IOException, GeneralSecurityException {
        logger.info("Invoking publish ....");

        publishHelper.publish();

        return ResponseEntity.ok("Uploaded");
    }
}
